package interfaz;
import Negocio.*;

import javax.swing.JFrame;
import javax.swing.JPanel;

import javax.swing.border.EmptyBorder;
import javax.swing.plaf.metal.MetalButtonUI;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.net.URL;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import javax.swing.ImageIcon;
import java.awt.Toolkit;

public class InterfazGraficaJuego extends JFrame {
	JButton btnNuevoJuego;
	JLabel lblcTurn;JLabel lblTurn;
	JLabel lblpO;JLabel lblpX;
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	JButton botones[] = new JButton[9];
	Juego tateti= new Juego();
	JLabel jugadas[] = new JLabel[12];
	URL direccion;

	public InterfazGraficaJuego(String jugadorX, String jugadorO) {
		direccion=getClass().getResource("../imagenes/");		
		setIconImage(Toolkit.getDefaultToolkit().getImage(direccion.getPath()+"tateti.ico"));
		setTitle("Tateti Toroidal");
		tateti.setNombres(jugadorX, jugadorO); 			
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		// LABELS JUGADAS
		for (int i=0;i<jugadas.length;i++) {
			jugadas[i]=new JLabel("");
			jugadas[i].setIcon(new ImageIcon(direccion.getPath()+"jugada"+(i+1)+".png"));
			jugadas[i].setVisible(false);
			contentPane.add(jugadas[i]);	
		}		
		jugadas[11].setBounds(-16, 73, 488, 412);jugadas[10].setBounds(-16, 11, 436, 459);
		jugadas[9].setBounds(30, 0, 529, 520);jugadas[8].setBounds(10, 63, 478, 453);
		jugadas[7].setBounds(-16, -72, 647, 577);jugadas[6].setBounds(0, -50, 488, 520);
		jugadas[5].setBounds(5, 0, 478, 427);jugadas[4].setBounds(0, 0, 472, 425);
		jugadas[3].setBounds(0, 0, 458, 427);jugadas[2].setBounds(5, 0, 478, 425);		
		jugadas[1].setBounds(5, 11, 513, 396);jugadas[0].setBounds(10, 11, 448, 396);		
		
		// PUNTAJEX: INT
		JLabel lblpuntajeX = new JLabel("Puntaje X: ");
		lblpuntajeX.setBounds(35, 11, 78, 22);
		contentPane.add(lblpuntajeX);
		
		lblpX = new JLabel("0");
		lblpX.setBounds(86, 15, 46, 14);
		contentPane.add(lblpX);

		// PUNTAJEO: INT
		JLabel lblpuntajeO = new JLabel("Puntaje O: ");
		lblpuntajeO.setBounds(380, 11, 78, 22);
		contentPane.add(lblpuntajeO);

		lblpO = new JLabel("0");
		lblpO.setBounds(434, 15, 46, 14);
		contentPane.add(lblpO);

		// TURNO: JUGADOR
		JLabel lblTurno = new JLabel("Turno de :");
		lblTurno.setBounds(35, 74, 65, 30);
		lblTurno.setFont(new Font("Tahoma", Font.PLAIN, 14));
		contentPane.add(lblTurno);

		lblTurn = new JLabel();
		lblTurn.setFont(new Font("Arial Black", Font.PLAIN, 14));
		lblTurn.setBounds(101, 74, 188, 30);
		contentPane.add(lblTurn);	
		lblTurn.setText(tateti.jugadorEnTurno());

		// CANTIDAD DE TURNOS: INT
		JLabel lblCantidadDeTurnos = new JLabel("Cantidad de turnos:");
		lblCantidadDeTurnos.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCantidadDeTurnos.setBounds(299, 74, 122, 30);
		contentPane.add(lblCantidadDeTurnos);

		lblcTurn = new JLabel("");
		lblcTurn.setFont(new Font("Arial Black", Font.PLAIN, 14));
		lblcTurn.setBounds(423, 74, 65, 30);
		contentPane.add(lblcTurn);

		// BUTTONS	
		int x=86;int y=115;int ancho=103;int alto=93;int dif=109; int j=0;
		for (int i = 0; i < botones.length; i++) {	
			botones[i]=new JButton("");
			botones[i].setOpaque(false);
			botones[i].setContentAreaFilled(false);
			botones[i].setFont(new Font("Arial Black", Font.PLAIN, 82));			
			botones[i].addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						for (int j = 0; j < botones.length; j++) {
							if (e.getSource() == botones[j]) {
								accion(j);
							}}}});
			contentPane.add(botones[i]);			
			botones[i].setBounds(x,y,ancho,alto);
			if(j<2) {
				x+=dif;j++;
			}else {
				x=86;y+=dif;j=0;
			}
		}	
		
		// Button NUEVO JUEGO
		btnNuevoJuego = new JButton("Nuevo Juego");
		btnNuevoJuego.setToolTipText("Reinicio de juego");
		btnNuevoJuego.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				reiniciarBotones();
				reiniciarJuego();		
				tateti.reiniciarPuntaje();
				actualizarPuntajes();
				lblTurn.setText(tateti.jugadorEnTurno());;
			}
		});
		btnNuevoJuego.setBounds(194, 41, 103, 30);
		contentPane.add(btnNuevoJuego);
		contentPane.setLayout(null);

		JLabel lblGrilla = new JLabel("");
		lblGrilla.setIcon(new ImageIcon(direccion.getPath()+"grilla.png"));
		lblGrilla.setVisible(true);
		lblGrilla.setBounds(79, 97, 331, 344);
		contentPane.add(lblGrilla);
	}
	public void reiniciarBotones() {
		for (JButton btn : botones) {
			btn.setText("");
			btn.setEnabled(true);
		}
	}
	public void rayar() {
		jugadas[tateti.jugadaGanadora() - 1].setVisible(true);
	}
	public void accion(int i) {	
		letra(botones[i]);
		String aux=tateti.jugadorEnTurno();
		tateti.cambiarTurno();
		lblcTurn.setText(String.valueOf(tateti.getCantidadTurnos()));		
		lblTurn.setText(tateti.jugadorEnTurno());
		tateti.getTablero().marcar(tateti.getTurno(), i);
		if (tateti.seGano()) {
			actualizarPuntajes();
			rayar();
			JOptionPane.showMessageDialog(null, "��El ganador es: " + aux + "!!");
			reiniciarBotones();
			reiniciarJuego();		
			reiniciarJugadas();
		}
	}
	public void reiniciarJugadas() {
		for (JLabel jugada : jugadas) {
			jugada.setVisible(false);
		}
	}
	public void letra(JButton boton) {
		String letra=tateti.letraEnTurno();
		if (letra.equals("X")) {				
			boton.setForeground(Color.red);
			boton.setUI(new MetalButtonUI() {
				protected Color getDisabledTextColor() {
					return Color.RED;
				}});
		} else {
			boton.setForeground(Color.blue);
			boton.setUI(new MetalButtonUI() {
				protected Color getDisabledTextColor() {
					return Color.BLUE;
				}});
		}				
		boton.setText(letra);
		boton.setEnabled(false);
	}
	
	public void reiniciarJuego(){
		tateti.reiniciarJuego();
		lblcTurn.setText(String.valueOf(tateti.getCantidadTurnos()));		
	}
	public void actualizarPuntajes() {
		lblpX.setText(String.valueOf(tateti.getJugadorX().getPuntaje()));
		lblpO.setText(String.valueOf(tateti.getJugadorO().getPuntaje()));
	}
}
